# Application-chat-multithread

## Introduction 

Nous avons créé une application de chat client-serveur basée sur des sockets, qui permet à un groupe de participants de discuter et d'interagir entre eux et les conversations sont affichées dans la console.

### Concept

Pour le concept du serveur, lorsqu'une connexion est établie, un nouveau thread est créé pour la gérer et diffuser les messages reçus à chaque client. Lorsqu'un client quitte la connexion, le thread ferme le socket correspondant, met à jour la table des informations de connexion.

Après avoir établi une connexion, chaque client vérifie le nom d'utilisateur saisi par l'utilisateur en envoyant une requête à la table d'informations de connexion. Une fois confirmé, deux threads sont ouverts pour gérer respectivement la lecture et l'écritur.

On a aussi implémenté les méthondes pour gérer les exceptions de connexion de socket client et serveur.

### Utilisation théorique

Le diagramme ci-dessous montre le diagramme de temps théorique pour la démonstration de chat que nous avons développée avec deux utilisateurs connectés, alors qu'ils communiquent l'un avec l'autre. Ce diagramme de temps est présenté dans l'ordre du haut vers le bas.

![2](./IMG/2.png)

## Explication

L'image ci-dessous montre le diagramme UML de la classe que nous avons conçue. La mise en œuvre de toutes les classes est expliquée en détail ci-dessous.

<img src="./IMG/1.png" alt="1" style="zoom: 33%;" />

### Server

Ce code est une implémentation d'un serveur de chat basé sur des sockets en Java. Il utilise la classe ServerSocket pour accepter les connexions entrantes et stocker les sockets des clients connectés dans un tableau associatif (`Map`). Le tableau stocke également les noms d'utilisateur correspondant à chaque socket client.

```java
/**
* Le tableau qui stocke les sockets de client et les noms d'utilisateur correspondant
*/
private final Map<String, Socket> clientsTable;
/**
* Construire un object Serveur, instancier le tableau des clients
*/
private Server() {
		clientsTable = new Hashtable<>();
}
```

On a choisi `Hashtable` comme structure de données car elle est thread-safe.

La classe Server utilise le pattern de conception `Singleton` pour s'assurer qu'il n'existe qu'une seule instance de la classe Server en cours d'exécution. Cette instance est créée lors de l'initialisation de la classe en tant que champ statique de la classe.

```java
/**
* L'objet de la classe Server instancié au moment de la création de classe
*/
private static final Server instance = new Server();
```

La classe Server contient plusieurs méthodes qui gèrent l'ajout et la suppression de clients dans le tableau associatif, ainsi que la diffusion de messages à tous les clients connectés. La méthode addClient ajoute un client au tableau associatif avec un nom d'utilisateur valide. La méthode deleteClient supprime un client du tableau associatif lorsque le client se déconnecte. La méthode broadcast envoie un message à tous les clients connectés.

```java
public boolean addClient(String username, Socket client) {
		Server server = Server.getInstance();
		// Vérifier si le nom d'utilisateur saisi est non null et n'existe pas dans le tableau
		if (!username.equals("") && !server.getClientsTable().containsKey(username)) {
				server.getClientsTable().put(username, client);
				return true;
		} else {
				return false;
		}
}
public void deleteClient(String username) {
    Server server = Server.getInstance();
    server.getClientsTable().remove(username);
}
public void broadcast(String msg) throws IOException {
    Server server = Server.getInstance();
    for (Map.Entry<String, Socket> entry : server.getClientsTable().entrySet()) {
        DataOutputStream outsData = new DataOutputStream(entry.getValue().getOutputStream());
        outsData.writeUTF(msg);
    }
}
```

La méthode main est la méthode principale du programme et est exécutée lorsque le serveur est démarré. Cette méthode crée une instance de ServerSocket pour accepter les connexions entrantes et boucle infiniment pour attendre les nouvelles connexions entrantes. Lorsqu'un nouveau client se connecte, la méthode `accept()` de ServerSocket est appelée pour accepter la connexion et créer un nouveau socket client. 

Ensuite, le nom d'utilisateur du client est vérifié pour s'assurer qu'il est valide, puis le client est ajouté au tableau associatif avec son nom d'utilisateur correspondant. Enfin, un thread est créé pour intercepter les messages envoyés par le client et les diffuser à tous les autres clients connectés.

```java
while (true) {
		username = insData.readUTF();
		System.out.println("Client input username: " + username);
				if (server.addClient(username, client)) {  // L'ajout de client réussi
						outsData.writeUTF("true");
						server.broadcast("    " + username + " a rejoint la conversation");
						System.out.println("Username valid, client joined: " + username);
						break;
				} else { // Le nom d'utilisateur est invalide
						outsData.writeUTF("false");
						System.out.println("Username invalid");
				}
}
```

### MessageReceptorServer

 La classe MessageReceptorServer étend la classe Thread et permet de recevoir les messages envoyés par un client et de les diffuser à tous les clients connectés au serveur.

Le constructeur de la classe prend en paramètres le nom d'utilisateur et le socket de client. Les méthodes `getClient()` et `setUsername()` permettent d'obtenir et de modifier le socket de client et le nom d'utilisateur respectivement.

La méthode `run()` est une méthode héritée de la classe Thread qui est exécutée lorsqu'un nouveau thread est démarré. Elle utilise un objet Server pour diffuser les messages reçus à tous les clients connectés au serveur. La méthode lit les données d'entrée à partir du socket du client à l'aide d'un objet `DataInputStream` et utilise une boucle while pour continuer à lire les messages jusqu'à ce que le client quitte la conversation avec le message "`exit`".

Si le message est "`exit`", le serveur diffuse un message à tous les clients connectés pour informer que l'utilisateur a quitté la conversation, ferme le socket de client, supprime l'utilisateur du tableau des clients et arrête le thread avec la méthode `join()`. Sinon, le serveur diffuse le message reçu à tous les clients connectés en incluant le nom de l'utilisateur qui a envoyé le message.

```java
while (true) {
		String msg = insData.readUTF();
		if (msg.equals("exit")) {
				server.broadcast("    " + getUsername() + " a quitté la conversation");
				break;
		} else {
				String str = "    " + getUsername() + "a dit : " + msg;
				server.broadcast(str);
		}
}
```

Si une exception `IOException` est levée lors de la lecture des données à partir du socket du client, cela indique que le client a été déconnecté de manière inattendue. Dans ce cas, le serveur diffuse un message à tous les clients connectés pour informer que l'utilisateur a quitté la conversation, supprime l'utilisateur du tableau des clients et affiche un message dans la console pour indiquer que le client a été déconnecté. Si une exception InterruptedException est levée pendant l'exécution de la méthode `join()`, cela indique qu'une interruption a été signalée pour ce thread.

```java
[...]
} catch (IOException ex) {
//            Logger.getLogger(MessageReceptorClient.class.getName()).log(Level.SEVERE, null, ex);
		try {
				server.broadcast("    " + getUsername() + " a quitté la conversation");
				server.deleteClient(getUsername());
				System.out.println("Client disconnected: " + getUsername() + ", client removed!");
		} catch (IOException e) {
				throw new RuntimeException(e);
		}
} catch (InterruptedException e) {
[...]
```

### Client

Le code présenté est une classe appelée `Client` qui permet de se connecter à un serveur de chat et de participer à une conversation en ligne.

Dans la méthode `main`, le programme commence par se connecter à un serveur sur `localhost` via le port 10080 en créant un nouveau socket `client`. Ensuite, le programme demande à l'utilisateur de saisir un nom d'utilisateur avec un scanner et l'envoie au serveur via le flux de sortie de données `outsData`.

Le programme attend ensuite une réponse du serveur quant à la validité du nom d'utilisateur saisi. Si le nom d'utilisateur est valide, le programme affiche un message indiquant que l'utilisateur a rejoint la conversation, et lance deux threads différents pour gérer l'envoi et la réception de messages. Sinon, le programme demande à l'utilisateur de saisir un nouveau nom d'utilisateur.

```java
while (true) {
		System.out.println("Entrez votre pseudo : ");
		Scanner scanner = new Scanner(System.in);
		username = scanner.next();
		outsData.writeUTF(username);
		// Vérifier la validité de nom d'utilisateur indiquée par le serveur
		addClientState = insData.readUTF();
    if (addClientState.equals("true")) {
				System.out.println("    " + username + " a rejoint la conversation");
				System.out.println("----------------------------");
				break;
		} else {
				System.out.println("Ce pseudo est déjà utilisé !");
		}
}
```

Le thread `MessageEmitterClient` intercepte les messages saisis par l'utilisateur et les envoie au serveur via le flux de sortie de données `outsData`. Le thread `MessageReceptorClient` reçoit les messages provenant du serveur via le flux d'entrée de données `insData` et les affiche à l'utilisateur. Les deux threads sont lancés avec la méthode `start()`.

```java
// Lancer le thread qui intercepte les messages saisis par l’utilisateur et les envoient au serveur
MessageEmitterClient msgEmitter = new MessageEmitterClient(username, client);
msgEmitter.start();
// Lancer le thread qui récupère les messages saisis par l’utilisateur et les transmet au serveur
MessageReceptorClient msgReceptor = new MessageReceptorClient(username, client);
msgReceptor.start();
```

En cas de problème de connexion avec le serveur, le programme affiche un message d'erreur approprié. Si l'un des threads de messagerie est interrompu (par exemple, si l'utilisateur quitte la conversation), le programme termine les threads et se termine.

### MessageEmitterClient

La classe `MessageEmitterClient` a deux attributs, le nom d'utilisateur et le socket de client, qui sont passés en paramètres lors de la construction de l'objet. Le nom d'utilisateur est utilisé pour identifier l'utilisateur qui envoie le message, tandis que le socket de client est utilisé pour envoyer les messages au serveur.

La classe a également une méthode `run()` qui est appelée lorsque le thread est démarré. Dans cette méthode, un objet `DataOutputStream` est créé à partir du socket client, ce qui permet d'envoyer des données au serveur.

Ensuite, une boucle `while(true)` est exécutée pour récupérer les messages saisis par l'utilisateur à partir de la console. Si le message n'est pas vide, le message est envoyé au serveur à l'aide de l'objet `DataOutputStream`. Si le message est `exit`, la boucle est interrompue et le thread se termine.

Enfin, la méthode `join()` est appelée pour attendre que le thread se termine. Cette méthode est appelée sur l'objet courant, ce qui signifie que le thread appelant attendra que le thread courant se termine avant de continuer son exécution.

### MessageReceptorClient

La classe `MessageReceptorClient` est une extension de la classe `Thread`. Elle possède deux variables d'instance privées, `username` et `client`, qui sont initialisées via son constructeur. 

La fonction `run()` est redéfinie pour obtenir les messages du serveur via la méthode `readUTF()` de l'objet `DataInputStream`. Les messages sont ensuite affichés dans la console du client à l'aide de `System.out.println()`. Si le message reçu indique que l'utilisateur a quitté la conversation, la boucle `while` est interrompue. Si une exception de type `IOException` est levée, un message d'erreur est affiché.

## Démonstration

### Récupération du code

```shell
git clone https://gitlab.utc.fr/shizhuze/sr03-application-chat-multithread.git
```

### Utilisation

#### Démonstration avec un serveur et un client

![3](./IMG/3.png)

#### Démonstration avec un serveur et deux clients 

![4](./IMG/4.png)

Dans cette démonstration, on démontre également la situation où le deuxième client saisi le même nom que celui du premier.

#### Démonstration avec le serveur interrompe 

![5](./IMG/5.png)

#### Démonstration avec le client interrompe 

![6](./IMG/6.png)

## Conclusion

En conclusion, la classe `Client` est la classe principale qui établit une connexion avec le serveur, crée un nom d'utilisateur et lance deux threads pour envoyer et recevoir des messages du serveur via les classes `MessageEmitterClient` et `MessageReceptorClient`. La classe `MessageEmitterClient` gère la saisie et l'envoi de messages de l'utilisateur au serveur, tandis que la classe `MessageReceptorClient` gère la réception et l'affichage des messages provenant du serveur. 

La classe `Server` est utilisée pour gérer la réception des messages en provenant des clients et l'entrée et la sortie des classes de clients.

Ensemble, ces classes fournissent une solution de chat simple mais fonctionnelle pour les utilisateurs qui se connectent au serveur.